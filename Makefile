phony: start-server install test stop-server

start-server:
	docker build -t apache target
	docker run -d --name apache -p 80:80 -p 443:443 apache

stop-server:
	docker kill apache & docker rm apache

test:
	go test -v

install:
	go install github.com/the-code-robot/crawllib/cmd/crawllib
	go install github.com/the-code-robot/crawllib/cmd/crawllib-api
