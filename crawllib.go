/*
crawllib - a web crawler written in Go.

Usage:
	crawllib http://tomblomfield.com  2> crawllib.log

Output is JSON structure of each page found and a
list of the links (both pages and assets) linked to from each page.

The logs contain debugging information and, at the end
a summary of all pages found and the total time taken.

To do
-----

* Honour robots.txt
* Implement publicsuffix.org for subdomain crawling
* Use goroutines to process each url concurrently
* Create a 'toCrawl' queue
* Limit search by sitemap length, not depth.
* More tests.

*/
package crawllib

import "encoding/json"
import "fmt"
import "io"
import "log"
import "net/http"
import "net/url"
import "os"
import "strings"
import "time"

import "golang.org/x/net/html"

type PageLinks struct {
	URL   string
	Links []string
}

var sitemap = make(map[string]PageLinks)
var toCrawl = make(map[string]string)

func main() {
	log.Printf("Starting crawllib Web Crawler")
	log.Printf("Command line arguments: %s", os.Args)
	root_url := os.Args[1]
	start := time.Now()
	sitemap = Crawl(root_url)
	b, _ := json.MarshalIndent(sitemap, " ", "   ")
	fmt.Printf("%s", b)
	log.Printf("Completed crawl: %s", time.Since(start))
	log.Printf("Found %s pages.", len(sitemap))
	count := 0
	for page := range sitemap {
		count += 1
		log.Printf("%s\t%s\n", count, page)
	}
}

func Crawl(url string) map[string]PageLinks {

	crawlSuggestions := make(chan string)
	toCrawl := make(chan string)
	resultsToAdd := make(chan PageLinks)
	go CrawlOffice(crawlSuggestions, toCrawl)
	go PageProcessor(url, toCrawl, resultsToAdd, crawlSuggestions)
	crawlSuggestions <- url
	return resultsProcessor(resultsToAdd)
}

func resultsProcessor(resultsToAdd chan PageLinks) map[string]PageLinks {
	results := make(map[string]PageLinks)
	lastResult := time.Now()
	var result PageLinks
	for {
		select {
		case result = <-resultsToAdd:
			lastResult = time.Now()
			//fmt.Println("\n\n\n", result)
			url := result.URL
			results[url] = result
		default:
			if time.Since(lastResult) > time.Second*3 {
				fmt.Printf("Tsl: %v", time.Since(lastResult))
				return results
			}
		}
	}
	return results
}

func PageProcessor(rurl string, toCrawl chan string, resultsToAdd chan PageLinks, crawlSuggestions chan string) {
	for {
		url := <-toCrawl
		go GetLinks(url, rurl, resultsToAdd, crawlSuggestions)
	}
}

func CrawlOffice(crawlSuggestions chan string, toCrawl chan string) {
	visited := make(map[string]string)
	for c := range crawlSuggestions {
		if _, ok := visited[c]; !ok {
			visited[c] = c
			toCrawl <- c
		}
	}
}

// Crawl pages to a maximum depth.
/*
func Crawl(RootURL string, PageURL string, depth int) {
	log.Printf("Crawling: %s, depth: %s\n", PageURL, depth)
	if depth <= 0 {
		log.Printf("Returning as depth <=0")
		return
	}
	urls := GetLinks(PageURL, ) //fetch & extract in one func call.
	log.Printf("found: %s \n", urls)
	count := 0
	for u := range urls {
		count += 1
		log.Printf("%s: Recursively crawling for: %s\n", count, u)
		if ValidLink(RootURL, u) {
			Crawl(RootURL, u, depth-1)
		} else {
			log.Printf("Skipping URL: %v", u)
			//return
		}
	}
	return
}
*/

/*
   puts pageLinks into the sitemap
*/
func GetLinks(url string, rurl string, resultsToAdd chan PageLinks, crawlSuggestions chan string) {
	response := Fetch(url)
	if response.StatusCode != 200 {
		log.Printf("Skipped: %v returned status code %v", url, response.StatusCode)
		return
	}
	ct := response.Header.Get("Content-Type")
	if !strings.Contains(ct, "text/html") {
		return
	}
	allUrls, crawlUrls2 := ExtractLinks(url, response.Body)
	a := make([]string, len(allUrls))
	count := 0
	for _, v := range allUrls {
		a[count] = v
		count += 1
	}
	s := PageLinks{URL: url, Links: a}
	resultsToAdd <- s
	for _, v := range crawlUrls2 {
		if ValidLink(rurl, v) {
			crawlSuggestions <- v
		}
	}
}

// Dirt simple for now: if domain not in host, blow up.
func ValidLink(startURL string, URL string) bool {
	for k := range sitemap {
		if k == URL {
			log.Printf("%s already in sitemap", k)
			return false
		}
	}
	return strings.Contains(URL, startURL)
}

// Just get the page.
func Fetch(url string) *http.Response {
	response, _ := http.Get(url)
	return response
}

/*
Build a document tree in memory then walk the tree looking for nodes where
links to other resources might be found.

Refactor to DRY ....
*/
func ExtractLinks(url string, reader io.Reader) (map[string]string, map[string]string) {
	result := make(map[string]string)
	crawls := make(map[string]string)
	doc, err := html.Parse(reader)
	if err != nil {
		// ...
	}
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "a" {
			v, _ := GetAttrVal("href", n.Attr)
			absLink := loseFragment(resolveLink(v, "", url))
			if absLink == "" {
				return
			}
			result[absLink] = absLink
			crawls[absLink] = absLink
		}
		if n.Type == html.ElementNode && n.Data == "img" {
			v, _ := GetAttrVal("src", n.Attr)
			absLink := resolveLink(v, "", url)
			if absLink == "" {
				return
			}
			result[absLink] = absLink
		}
		if n.Type == html.ElementNode && n.Data == "script" {
			v, _ := GetAttrVal("src", n.Attr)
			absLink := resolveLink(v, "", url)
			if absLink == "" {
				return
			}
			result[absLink] = absLink
		}
		if n.Type == html.ElementNode && n.Data == "link" {
			v, _ := GetAttrVal("href", n.Attr)
			absLink := resolveLink(v, "", url)
			if absLink == "" {
				return
			}
			result[absLink] = absLink
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return result, crawls
}

// Create custom error for attribute not found
type AttrError struct {
	Key string
}

func (e *AttrError) Error() string {
	return fmt.Sprintf("Could find no key: %s", e.Key)
}

func GetAttrVal(k string, attr []html.Attribute) (string, error) {
	for _, a := range attr {
		if a.Key == k {
			return a.Val, nil
		}
	}
	return "", &AttrError{k}
}

// Make proper links from relative links, honouring base element if present.
func resolveLink(URLFromDocument string, baseURL string, requestingURL string) string {
	// if is absolute link, then return it.
	ufd, _ := url.Parse(URLFromDocument)
	if ufd.IsAbs() {
		return URLFromDocument
	}
	// if has baseURL use it
	if baseURL != "" {
		return baseURL + "/" + URLFromDocument
	}
	// otherwise use requesting URL
	ref, _ := url.Parse(requestingURL)
	return ref.ResolveReference(ufd).String()
}

// We don't need fragment identifiers.
func loseFragment(u string) string {
	h := strings.Index(u, "#")
	if h == -1 {
		return u
	}
	return u[:h]
}
