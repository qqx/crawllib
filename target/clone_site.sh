wget \
     --recursive \
     --no-clobber \
     --page-requisites \
     --html-extension \
     --convert-links \
     --restrict-file-names=windows \
     --no-parent \
     --directory-prefix=www.sitemaps.org \
         www.sitemaps.org

#     --domains sitemap.org \
