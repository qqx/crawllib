package crawllib

import "fmt"
import "io"
import "reflect"
import "strings"
import "testing"

import "golang.org/x/net/html"

func TestCrawlOffice(t *testing.T) {
	crawlSuggestions := make(chan string)
	toCrawl := make(chan string)
	go CrawlOffice(crawlSuggestions, toCrawl)
	crawlSuggestions <- "abc"
	got := <-toCrawl
	if got != "abc" {
		t.Errorf("Didn't get abc.")
	}
	crawlSuggestions <- "abc"
	crawlSuggestions <- "abcd"
	got2 := <-toCrawl
	fmt.Printf("got2: %s", got2)

}

// test GetAttrVal
func GAVTestHelper(t *testing.T, k string, attr []html.Attribute, want string, wanterr *AttrError) {
	got, err := GetAttrVal(k, attr)
	// check the errors are the same type
	if wanterr != nil {
		if reflect.TypeOf(wanterr) != reflect.TypeOf(err) {
			t.Errorf("Boom A")
		}
	} else {
		if err != nil {
			t.Errorf("Boom B")
		} else {
			if got != want {
				t.Errorf("Boom C")
			}
		}
	}
}

func TestMissingKeyGetAttrVal(t *testing.T) {
	attr1 := html.Attribute{Key: "href", Val: "about.html"}
	attrs := []html.Attribute{attr1}
	GAVTestHelper(t, "xhref", attrs, "Who cares? The key does not exist", &AttrError{"xhref"})
}

func TestKeyPresentGetAttrVal(t *testing.T) {
	attr1 := html.Attribute{Key: "href", Val: "about.html"}
	attrs := []html.Attribute{attr1}
	GAVTestHelper(t, "href", attrs, "about.html", nil)
}

// test resolveLink
func RLTestHelper(t *testing.T, URLFromDocument string, baseURL string, requestingURL string, want string) {
	got := resolveLink(URLFromDocument, baseURL, requestingURL)
	if got != want {
		t.Errorf("Got %s, want %s, URLFromDocument: %s, baseURL: %s, requestingURL: %s", got, want, URLFromDocument, baseURL, requestingURL)
	}
}

func TestRelativeResolveLink(t *testing.T) {
	URLFromDocument := "about.html"
	baseURL := ""
	requestingURL := "http://qqq.example.com"
	want := "http://qqq.example.com/about.html"
	RLTestHelper(t, URLFromDocument, baseURL, requestingURL, want)
}

func TestBaseResolveLink(t *testing.T) {
	URLFromDocument := "about.html"
	baseURL := "http://www.example.com"
	requestingURL := "http://qqq.example.com"
	want := "http://www.example.com/about.html"
	RLTestHelper(t, URLFromDocument, baseURL, requestingURL, want)
}

func TestAbsoluteResolveLink(t *testing.T) {
	URLFromDocument := "http://www.example.com/about.html"
	baseURL := "http://www.example.com"
	requestingURL := "http://qqq.example.com"
	want := "http://www.example.com/about.html"
	RLTestHelper(t, URLFromDocument, baseURL, requestingURL, want)
}

// test ValidLink
func testHelper(t *testing.T, startURL string, URL string, want bool) {
	got := ValidLink(startURL, URL)
	if got != want {
		t.Errorf("Got %s, wanted %s for ValidLink(%s, %s)", got, want, startURL, URL)

	}
}

var startURL string = "http://tomblomfield.com"

func TestValidLinkGood(t *testing.T) {
	testHelper(t, startURL, "http://tomblomfield.com/about.html", true)
}
func TestValidLinkBad(t *testing.T) {
	testHelper(t, startURL, "http://NOTtomblomfield.com/about.html", false)
}

func TestFetch(t *testing.T) {

	content := Fetch("http://localhost")
	b := make([]byte, 8)
	for {
		n, err := content.Body.Read(b)
		fmt.Printf("%s", b[:n])
		if err == io.EOF {
			break
		}
	}
}

func TestExtractLinks(t *testing.T) {

	url := "http://localhost"
	html := `<html><head><title>Test Page</title></head><body><h1>News</h1><p>I was walking <a href="about.html">home</a>.<img src="image.jpg" /></p></body></html>`
	htmlreader := strings.NewReader(html)
	alllinks, _ := ExtractLinks(url, htmlreader)
	for _, val := range alllinks {
		fmt.Printf("val: %s\n", val)
	}

}

/*
func TestCrawl(t *testing.T) {
	url := "http://localhost"
	Crawl(url, url, 4)

}
*/

func TestLoseFragment(t *testing.T) {
	u := "http://www.example.com#abc"
	want := "http://www.example.com"
	got := loseFragment(u)
	if got != want {
		t.Errorf("got %s wanted %s for input %s", got, want, u)
	}
}
