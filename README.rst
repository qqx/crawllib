crawllib
========

Pre-requisites
--------------

Production
..........

* go

Testing
.......

* make   
* wget
* Docker

Installation
------------

::
    make start-server
    make test
    make install

Testing
-------
A helper script target/clone_site.sh will, if you edit it
copy a site which can then be served by the Apache running in the docker container.

Run::

   cd target/content/html
   sh ../../clone_site.sh

Check that the site's pages are visible at ./target/content/html. View the Dockerfile for details.

Rebuild the docker image using::

   make stop-server
   make start-server

to pick up the changes.

Then::

    make install
    crawllib http://localhost


Usage
-----

crawllib http://localhost 2> crawllib.log

This will print a JSON formatted sitemap to stdout.

Debugging and performance metrics are in the logs.


Todo
----
Refer to the top of crawllib.go for the list of outstanding refactorings.
