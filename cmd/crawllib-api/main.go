/*
A microservice api
*/
package main

import (
	"encoding/json"
	"fmt"
	"github.com/the-code-robot/crawllib"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Hello from crawllib-api")
	http.Handle("/", http.HandlerFunc(crawl))
	addr := ":8088"
	err := http.ListenAndServe(addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

func crawl(w http.ResponseWriter, req *http.Request) {
	url := req.FormValue("crawlurl")
	log.Printf("url: %s\n", url)
	sitemap := crawllib.Crawl(url)
	b, _ := json.MarshalIndent(sitemap, " ", "   ")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
}
