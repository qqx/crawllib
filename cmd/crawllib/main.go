/*
The Monzo take home programming test.

Usage:
	monzo http://tomblomfield.com  2> monzo.log

Output is JSON structure of each page found and a
list of the links (both pages and assets) linked to from each page.

The logs contain debugging information and, at the end
a summary of all pages found and the total time taken.

To do
-----

* Honour robots.txt
* Implement publicsuffix.org for subdomain crawling
* Use goroutines to process each url concurrently
* Create a 'toCrawl' queue
* Limit search by sitemap length, not depth.
* More tests.

*/
package main

import (
	"encoding/json"
	"fmt"
	"github.com/the-code-robot/crawllib"
	"log"
	"os"
	"time"
)

var sitemap = make(map[string]crawllib.PageLinks)

func main() {
	log.Printf("Starting Monzo Web Crawler")
	log.Printf("Command line arguments: %s", os.Args)
	root_url := os.Args[1]
	start := time.Now()
	sitemap = crawllib.Crawl(root_url)
	b, _ := json.MarshalIndent(sitemap, " ", "   ")
	fmt.Printf("%s", b)
	log.Printf("Completed crawl: %s", time.Since(start))
	log.Printf("Found %s pages.", len(sitemap))
	count := 0
	for page := range sitemap {
		count += 1
		log.Printf("%s\t%s\n", count, page)
	}
}
